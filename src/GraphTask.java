import java.util.*;
import java.util.Collections;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 * @since 1.8
 */
public class GraphTask {

   enum Color {white, gray, black};
   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
//      Graph g = new Graph ("G");
//      g.createRandomSimpleGraph (10, 13);
//      System.out.println (g);
//      g.breadthFirstSearch("v1");
      // TODO!!! Your experiments here
      //test 1
      Graph g1 = new Graph ("G1 -------------");
      g1.testGraph1();
      System.out.println (g1);
      List<List<Arc>> arcList1 = g1.breadthFirstSearch("v1");
      System.out.println ("");
      //test 2
      Graph g2 = new Graph("G2 -------------");
      g2.testGraph2();
      System.out.println(g2);
      List<List<Arc>> arcList2 = g2.breadthFirstSearch("v1");
      //test 3
      Graph g3 = new Graph("G3 -------------");
      g3.testGraph3();
      System.out.println(g3);
      List<List<Arc>> arcList3 = g3.breadthFirstSearch("v1");
      //test 4
      Graph g4 = new Graph("G4 -------------");
      g4.testGraph4();
      System.out.println(g4);
      List<List<Arc>> arcList4 = g4.breadthFirstSearch("v4");
      //test 5
      Graph g5 = new Graph("G5 -------------");
      g5.testGraph5();
      System.out.println(g5);
      List<List<Arc>> arcList5 = g5.breadthFirstSearch("v1");

      System.out.println("");
      System.out.println("");
      //time test
      Graph g6 = new Graph("G6 -----------");
      g6.createRandomSimpleGraph (2000, 2500);
      long start = System.nanoTime();
      g6.breadthFirstSearch("v15");
      long end = System.nanoTime();
      System.out.println("Elapsed Time in nano seconds: "+ (end-start));

      System.out.println("");
      System.out.println("");
   }

   // TODO!!! add javadoc relevant to your problem
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      public Color color = Color.white;
      public int d;
      public Vertex u;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Vertex methods here!
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
   }


   /** This header represents a graph.
    */
   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed
      private int[][] adjMatrix;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j)
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
         adjMatrix = connected;
      }

      // TODO!!! Your Graph methods here! Probably your solution belongs here.


      /**
       * test1
       * v1 --> av1_av2(v1_v2) av1_av3(v1_v3)
       * v2 --> av2_av1(v2_v1) av2_av5(v2_v5)
       * v3 --> av3_av1(v3_v1) av3_av4(v3_v4)
       * v4 --> av4_av3(v4_v3)
       * v5 --> av5_av2(v5_v2) av5_av6(v5_v6)
       * v6 --> av6_av5(v6_v5)
       *
       * The most far vertex is v6 from v1 with the arcs
       * av1_v2 --> av2_v5 --> av5_v6
       */
      public void testGraph1() {
         int[][] adjMat = {
                 {0, 1, 1, 0, 0, 0},
                 {1, 0, 0, 0, 1, 0},
                 {1, 0, 0, 1, 0, 0},
                 {0, 0, 1, 0, 0, 0},
                 {0, 1, 0, 0, 0, 1},
                 {0, 0, 0, 0, 1, 0}
         };

         adjMatrix2Graph(adjMat);
      }

      /**
       * test2
       * v1 --> av1_av2(v1_v2) av1_av3(v1_v3)
       * v2 --> av2_av1(v2_v1) av2_av5(v2_v5)
       * v3 --> av3_av1(v3_v1)
       * v4 --> av4_av2(v4_v2)
       * v5 --> av5_av2(v5_v2) av5_av7(v5_v7)
       * v6 --> av6_av2(v6_v2) av6_av7(v6_v7)
       * v7 --> av7_av5(v7_v5) av7_av6(v7_v6)
       *
       * The most far vertex is v7 from v1 with the arcs
       * av1_v2 --> av2_v6 --> av6_v7
       */
      public void testGraph2() {
         int[][] adjMat = {
                 //1 2  3  4  5  6  7
                 {0, 1, 1, 0, 0, 0, 0}, //1
                 {1, 0, 0, 0, 1, 1, 0}, //2
                 {1, 0, 0, 0, 0, 0, 0}, //3
                 {0, 1, 0, 0, 0, 0, 0}, //4
                 {0, 1, 0, 0, 0, 0, 1}, //5
                 {0, 1, 0, 0, 0, 0, 1}, //6
                 {0, 0, 0, 0, 1, 1, 0}  //7
         };

         adjMatrix2Graph(adjMat);
      }

      /**
       * test3 neural network shape
       * v1 --> av1_av2(v1_v2) av1_av3(v1_v3) av1_av4(v1_v4)
       * v2 --> av2_av1(v2_v1) av2_av5(v2_v5) av2_av6(v2_v6) av2_av7(v2_v7)
       * v3 --> av3_av1(v3_v1) av3_av5(v3_v5) av3_av6(v3_v6) av3_av7(v3_v7)
       * v4 --> av4_av1(v4_v1) av4_av5(v4_v5) av4_av6(v4_v6) av4_av7(v4_v7)
       * v5 --> av5_av2(v5_v2) av5_av3(v5_v3) av5_av4(v5_v4) av5_av8(v5_v8)
       * v6 --> av6_av2(v6_v2) av6_av3(v6_v3) av6_av4(v6_v4) av6_av8(v6_v8)
       * v7 --> av7_av2(v7_v2) av7_av3(v7_v3) av7_av4(v7_v4) av7_av8(v7_v8)
       * v8 --> av8_av5(v8_v5) av8_av6(v8_v6) av8_av7(v8_v7)
       *
       * The most far vertex from v1 is v8 with the arcs
       * av1_v4 --> av4_v7 --> av7_v8
       */
      public void testGraph3() {
         int[][] adjMat = {
                 //1 2  3  4  5  6  7  8
                 {0, 1, 1, 1, 0, 0, 0, 0}, //1
                 {1, 0, 0, 0, 1, 1, 1, 0}, //2
                 {1, 0, 0, 0, 1, 1, 1, 0}, //3
                 {1, 0, 0, 0, 1, 1, 1, 0}, //4
                 {0, 1, 1, 1, 0, 0, 0, 1}, //5
                 {0, 1, 1, 1, 0, 0, 0, 1}, //6
                 {0, 1, 1, 1, 0, 0, 0, 1}, //7
                 {0, 0, 0, 0, 1, 1, 1, 0}  //8
         };

         adjMatrix2Graph(adjMat);
      }

      /**
       * test4 hexagon shape
       * v1 --> av1_av2(v1_v2) av1_av3(v1_v3) av1_av4(v1_v4)
       * v2 --> av2_av1(v2_v1) av2_av4(v2_v4) av2_av5(v2_v5)
       * v3 --> av3_av1(v3_v1) av3_av4(v3_v4) av3_av7(v3_v7)
       * v4 --> av4_av1(v4_v1) av4_av2(v4_v2) av4_av3(v4_v3) av4_av5(v4_v5) av4_av6(v4_v6) av4_av7(v4_v7)
       * v5 --> av5_av2(v5_v2) av5_av4(v5_v4) av5_av6(v5_v6)
       * v6 --> av6_av4(v6_v4) av6_av5(v6_v5) av6_av7(v6_v7)
       * v7 --> av7_av3(v7_v3) av7_av4(v7_v4) av7_av6(v7_v6)
       *
       * The most far vertex are v1, v2, v3, v5, v6, v7 from v4 with the arcs
       * av4_v1, av4_v2, av4_v3, av4_v5, av4_v6, av4_v7
       */
      public void testGraph4() {
         int[][] adjMat = {
                 //1 2  3  4  5  6  7
                 {0, 1, 1, 1, 0, 0, 0}, //1
                 {1, 0, 0, 1, 1, 0, 0}, //2
                 {1, 0, 0, 1, 0, 0, 1}, //3
                 {1, 1, 1, 0, 1, 1, 1}, //4
                 {0, 1, 0, 1, 0, 1, 1}, //5
                 {0, 0, 0, 1, 1, 0, 1}, //6
                 {0, 0, 1, 1, 0, 1, 0}  //7
         };

         adjMatrix2Graph(adjMat);
      }

      /**
       * test5
       * v1 --> av1_av2(v1_v2) av1_av3(v1_v3)
       * v2 --> av2_av1(v2_v1) av2_av4(v2_v4)
       * v3 --> av3_av1(v3_v1) av3_av1(v3_v4) av3_av5(v3_v5)
       * v4 --> av4_av2(v4_v2) av4_av3(v4_v3) av4_av9(v4_v9)
       * v5 --> av5_av3(v5_v3) av5_av6(v5_v6)
       * v6 --> av6_av5(v6_v5) av6_av7(v6_v7)
       * v7 --> av7_av8(v7_v8) av7_av13(v7_v13)
       * v8 --> av8_av7(v8_v7) av8_av9(v8_v9) av8_av10(v8_v10) av8_av11(v8_v11)
       * v9 --> av9_av4(v9_v4) av9_av8(v9_v8) av9_av10(v9_v10)
       * v10 --> av10_av8(v10_v8) av10_av9(v10_v9)
       * v11 --> av11_av8(v11_v8) av11_av10(v11_v10) av11_av12(v11_v12)
       * v12 --> av12_av11(v12_v11)
       * v13 --> av13_av7(v13_v7)
       *
       * The most far vertex from v1 are v12, v13 with the arcs
       * av1_v3 --> av3_v4 --> av4_v9 --> av9_v10 --> av10_v11 --> av11_v12
       * av1_v3 --> av3_v4 --> av4_v9 --> av9_v10 --> av10_v11 --> av11_v12
       */
      public void testGraph5() {
         int[][] adjMat = {
                 //1 2  3  4  5  6  7  8  9 10 11 12 13
                 {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //1
                 {1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //2
                 {1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0}, //3
                 {0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0}, //4
                 {0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0}, //5
                 {0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0}, //6
                 {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1}, //7
                 {0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0}, //8
                 {0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0}, //9
                 {0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0}, //10
                 {0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0}, //11
                 {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0}, //12
                 {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}, //13
         };

         adjMatrix2Graph(adjMat);
      }

      /**
       * Create graph from adjacency matrix
       * @param adjMat (binary)
       */
      private void adjMatrix2Graph(int[][] adjMat) {
         Vertex[] vertices = new Vertex[adjMat.length];
         vertices[0] = new Vertex("v1");
         first = vertices[0];
         for (int i=1; i<adjMat.length; i++) {
            vertices[i] = createVertex("v" + String.valueOf(i+1));
         }


         for (int i=0; i<adjMat.length; i++) {
            for (int j=0; j<i; j++) {
               if (adjMat[i][j] == 1) {
                  //create connection(arc)
                  Vertex v1 = vertices[i]; //row vertex
                  Vertex v2 = vertices[j]; //col vertex
                  createArc("a" + v1.toString() + "_" + v2.toString(), v1, v2);
                  createArc("a" + v2.toString() + "_" + v1.toString(), v2, v1);
               }
            }
         }
      }


      /**
       * To find adjacency-list for given vertex v
       * @param v Vertex
       * @return list List of vertices, they are adjacency vertices for given vertex.
       */
      private List<Vertex> getAdjacencyListForVertex(Vertex v) {
         List<Vertex> list = new ArrayList<Vertex>();
         Arc a = v.first;
         while (a != null) {
            list.add(a.target);
            a = a.next;
         }
         return list;
      }

      /**
       * Implementation of Breadth-First search algorithm to find distance
       * to each vertex from given vertex.
       * @param sourceId given vertex's id
       * @return List<List<Arc>> multiple list of arcs since the most far vertex can be multiple.
       * Referring "Introduction to algorithm 3rd edition pseudocode (page 589 ~ 600).
       */
      public List<List<Arc>> breadthFirstSearch (String sourceId) {
         Vertex s = new Vertex("");
         Vertex v = first;
         while (v != null) {
            if (v.id.equals(sourceId)) {
               s = v;
               break;
            }
            v = v.next;
         }
         if (s.id.isEmpty())
            throw new IllegalArgumentException ("Given source id: " + sourceId + " is not found in the graph");

         Queue queue = new LinkedList<Vertex>();
         s.color = Color.gray;
         s.d = 0;
         s.u = null;
         queue.add(s);


         int maxPathLength = 0;
         List<Vertex> list;
         List<Vertex> verticesCandidates = new ArrayList<Vertex>();

         int prevQueueSize = queue.size();
         while (queue.size() > 0) {
            Vertex u = (Vertex) queue.remove();
            list = getAdjacencyListForVertex(u);
            for (Vertex ver: list) {
               if (ver.color == Color.white) {
                  ver.color = Color.gray;
                  ver.d = u.d + 1;
                  ver.u = u;
                  queue.add(ver);
                  if (maxPathLength <= ver.d) {
                     maxPathLength = ver.d;
                  }
                  if (prevQueueSize <= queue.size()) {
                     verticesCandidates = new ArrayList(queue);
                  }
               }
            }
            prevQueueSize = queue.size();
            u.color = Color.black;
         }

         List<Vertex> farVertices = getFarVerticesFromList(verticesCandidates, maxPathLength);
         int count = 0;
         List<List<Arc>> arcLists = new ArrayList<List<Arc>>();

         System.out.print("The longest path from vertex, " + s.id);
         if (farVertices.size() > 1) {
            System.out.println(" are the followings;");
            for (Vertex ver: farVertices) {
               System.out.print(ver + " : ");
               List<Arc> arcList = new ArrayList<Arc>();
               arcList = getArcs(ver, arcList);
               Collections.reverse(arcList);
               arcLists.add(arcList);
               for (Arc a: arcList) {
                  if (count != arcList.size() -1) {
                     System.out.print(a + " --> ");
                     count++;
                  } else {
                     System.out.print(a);
                  }
               }
               System.out.println("");
               count = 0;
            }
         } else {
            for (Vertex ver: farVertices) {
               System.out.println(" is " + ver + " : ");
               List<Arc> arcList = new ArrayList<Arc>();
               arcList = getArcs(ver, arcList);
               Collections.reverse(arcList);
               arcLists.add(arcList);
               for (Arc a: arcList) {
                  if (count != arcList.size() -1) {
                     System.out.print(a + " --> ");
                     count++;
                  } else {
                     System.out.print(a);
                  }
               }
            }
         }
         System.out.println("\nThe max path length is " + maxPathLength);
         return arcLists;
      }

      /**
       * Finding the arc from v2 to v1 and accumulate to the list given.
       * @param v1 vertex further from the given vertex
       * @param v2 vertex closer to the given vertex
       * @return The arc connecting given two vertices.
       */
      private Arc findArc(Vertex v1, Vertex v2) {
         Arc arc = v2.first;

         while (arc != null) {
            if (arc.target == v1) return arc;
            arc = arc.next;
         }

         throw new IllegalArgumentException("There is no arc between " + v1 + " and " + v2);
      }

      /**
       * @param v vertex further from the given vertex
       * @param list initially empty vertex list.
       *             By recursion, it accumulates the arcs
       *             from the furthest vertex to the given vertex.
       * @return List of vertex. This list is reverse order.
       *         So, it needs to be reversed in the end.
       */
      private List<Arc> getArcs(Vertex v, List<Arc> list) {
         Vertex u = v.u;
         if (u != null) {
            list.add(findArc(v, u));
            getArcs(u, list);
         }
         return list;
      }

      private List<Vertex> getFarVerticesFromList(List<Vertex> list, int maxPathLength) {
         List<Vertex> vertices = new ArrayList<Vertex>();
         for (Vertex v: list) {
            if (v.d == maxPathLength) {
               vertices.add(v);
            }
         }
         return vertices;
      }

   }
}

